package com.cloudera.sa.quintiles.mapreduce.reducer;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class OneToOneReducer extends Reducer<Text, Text, Text, Text>{

  public static final Log LOG = LogFactory.getLog(OneToOneReducer.class);

  @Override
  protected void reduce(Text key, Iterable<Text> value, Context context) throws IOException, InterruptedException {
    LOG.info("========================================= Reducing once =========================================");
    for (Text text: value) {
      context.write(key, text);
    }
  }
}
