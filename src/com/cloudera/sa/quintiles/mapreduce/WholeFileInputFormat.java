package com.cloudera.sa.quintiles.mapreduce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.InvalidInputException;
import org.apache.hadoop.mapreduce.security.TokenCache;

public class WholeFileInputFormat extends FileInputFormat<NullWritable, Text> {
  @Override
  protected boolean isSplitable(JobContext context, Path file) {
    return false;
  }

  @Override
  protected long computeSplitSize(long blockSize, long minSize, long maxSize) {
    // TODO Auto-generated method stub
    return super.computeSplitSize(blockSize, minSize, maxSize);
  }

  @Override
  public RecordReader<NullWritable, Text> createRecordReader(InputSplit split,
      TaskAttemptContext context) throws IOException, InterruptedException {
    WholeFileRecordReader reader = new WholeFileRecordReader();
    reader.initialize(split, context);
    return reader;
  }

  @Override
  /** List input files. Assumption is that we are passing ONLY files... Filters are disabled!
   *
   * @param job the job to list input paths for
   * @return array of FileStatus objects
   * @throws IOException if zero items.
   */
  protected List<FileStatus> listStatus(JobContext job) throws IOException {
    List<FileStatus> result = new ArrayList<FileStatus>();
    Path[] dirs = getInputPaths(job);
    if (dirs.length == 0) {
      throw new IOException("No input paths specified in job");
    }

    // get tokens for all the required FileSystems..
    TokenCache.obtainTokensForNamenodes(job.getCredentials(), dirs,
                                        job.getConfiguration());

    List<IOException> errors = new ArrayList<IOException>();

    FileSystem fs = null;
    for (int i=0; i < dirs.length; ++i) {
      Path p = dirs[i];
      if (fs == null)
        fs = p.getFileSystem(job.getConfiguration());
      FileStatus[] matches = fs.listStatus(p);
      if (matches == null) {
        errors.add(new IOException("Input path does not exist: " + p));
      } else if (matches.length == 0) {
        errors.add(new IOException("Input Pattern " + p + " matches 0 files"));
      } else {
        for (FileStatus globStat: matches) {
          if (globStat.isDirectory()) {
            for(FileStatus stat: fs.listStatus(globStat.getPath())) {
              result.add(stat);
            }
          } else {
            result.add(globStat);
          }
        }
      }
    }

    if (!errors.isEmpty()) {
      throw new InvalidInputException(errors);
    }
    System.out.println (new Date () + " WholeFileInputFormat - Total input paths to process : " + result.size());
    return result;
  }
}