package com.cloudera.sa.quintiles.mapreduce.mapper;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

public class XMLToSequenceFileMapper extends Mapper<NullWritable, Text, Text, Text> {

  /**
   * Map the entire into into a single sequence in the output, using the input file name as the key
   */
  protected void map(NullWritable key, Text value, Context context) throws IOException,
      InterruptedException {
    context.write(new Text(((FileSplit)context.getInputSplit()).getPath().toString()), value);
  }
}
