package com.cloudera.sa.quintiles.mapreduce.mapper;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.openhealthtools.mdht.uml.cda.Author;
import org.openhealthtools.mdht.uml.cda.Observation;
import org.openhealthtools.mdht.uml.cda.PatientRole;
import org.openhealthtools.mdht.uml.cda.Section;
import org.openhealthtools.mdht.uml.cda.consol.ConsolPackage;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.cda.consol.ProceduresSection;
import org.openhealthtools.mdht.uml.cda.util.CDAUtil;
import org.openhealthtools.mdht.uml.cda.util.ValidationResult;
import org.openhealthtools.mdht.uml.hl7.datatypes.ANY;
import org.openhealthtools.mdht.uml.hl7.datatypes.CE;
import org.openhealthtools.mdht.uml.hl7.datatypes.CS;
import org.openhealthtools.mdht.uml.hl7.datatypes.PN;
import org.openhealthtools.mdht.uml.hl7.datatypes.impl.CDImpl;
import org.openhealthtools.mdht.uml.hl7.datatypes.impl.IVL_TSImpl;
import org.openhealthtools.mdht.uml.hl7.datatypes.impl.STImpl;

public class XMLToOMOPMapper extends Mapper<Text, Text, Text, Text> {

	public static final SimpleDateFormat dateOfBirthFormat = new SimpleDateFormat(
			"yyyyMMdd");
	public static final Log LOG = LogFactory.getLog(XMLToOMOPMapper.class);

	private MultipleOutputs<Text, Text> multipleOutputs;

	public static enum MATCH_COUNTER {
		DOCUMENT, FAILED_DOCUMENT
	};

	protected static String getPatientInfo(EList<PN> names) {
		String givenName = "";
		String familyName = "";

		if (names.size() > 0) {
			if (names.size() > 1) {
				// Log a warning
			}
			// Take a maximum of 2 given names.
			for (int indexGivenName = 0; indexGivenName < Math.min(2, names
					.get(0).getGivens().size()); indexGivenName++) {
				givenName += names.get(0).getGivens().get(indexGivenName)
						.getText()
						+ " ";
			}
			// Take only the first family name.
			if (names.get(0).getFamilies().size() > 0)
				familyName = names.get(0).getFamilies().get(0).getText(); // Take
																			// only
																			// the
																			// first
		}
		return givenName + "\t" + familyName;
	}

	@Override
	protected void setup(Context context) throws IOException,
			InterruptedException {
		// TODO Auto-generated method stub
		super.setup(context);
		LOG.info("========================================= Setting up mapper =========================================");
		ConsolPackage.eINSTANCE.eClass();
		multipleOutputs = new MultipleOutputs<Text, Text>(context);
	}

	protected static ContinuityOfCareDocument getDocumentFromStream(
			InputStream is) throws Exception {
		try {
			// return (ContinuityOfCareDocument) CDAUtil.load(is);
			return (ContinuityOfCareDocument) CDAUtil.load(is,
					new ValidationResult());
		} catch (Exception e) {
			LOG.error("Unable to parse XML document", e);
		}
		return null;
	}

	protected static boolean validateDocument(
			ContinuityOfCareDocument continuityOfCareDocument) throws Exception {
		return CDAUtil.validate(continuityOfCareDocument,
				new ValidationResult(), true);
		// return CDAUtil.validate(continuityOfCareDocument);
	}

	public static String getCode(CE value) {
		if (value == null)
			return "";
		else
			return value.getCode();
	}

	protected static String getAuthorInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		try {
			EList<Author> authors = continuityOfCareDocument.getAuthors();
			for (Author author : authors) {
				return author.getAssignedAuthor().getAddrs().get(0).getStates()
						.get(0).getText();
			}
		} catch (Throwable t) {
			LOG.error("Unable to parse content from XML file", t);
		}
		return null;
	}

	public static String getCareSiteInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0;
	}

	public static String getCohortInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + 0 + "\01" + "2014-02-05 10:10:10" + "\01"
				+ "2014-02-05 10:10:10" + "\01" + 0 + "\01" + 0;
	}

	public static String getConditionEraInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + 0 + "\01" + 0 + "\01" + "2014-02-05 10:10:10"
				+ "\01" + "2014-02-05 10:10:10" + "\01" + 0 + "\01" + 0;
	}

	public static String getConditionOccurenceInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + 0 + "\01" + 0 + "\01" + "2014-02-05 10:10:10"
				+ "\01" + "2014-02-06 10:10:10" + "\01" + 0 + "\01" + 0 + "\01"
				+ 0 + "\01" + 0 + "\01" + 0;
	}

	public static String getDeathInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + "2014-02-05 10:10:10" + "\01" + 0 + "\01" + 0
				+ "\01" + 0;
	}

	public static String getDrugCostInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0
				+ "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0
				+ "\01" + 0 + "\01" + 0;
	}

	public static String getDrugEraInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + 0 + "\01" + 0 + "\01" + "2014-02-06 10:10:10"
				+ "\01" + "2014-02-06 10:10:10" + "\01" + 0 + "\01" + 0;
	}

	public static String getDrugExposureInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + 0 + "\01" + 0 + "\01" + "2014-02-06 10:10:10"
				+ "\01" + "2014-02-06 10:10:10" + "\01" + 0 + "\01" + 0 + "\01"
				+ 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0
				+ "\01" + 0 + "\01" + 0;
	}

	public static String getLocationInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0
				+ "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0
				+ "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0;
	}

	public void proceedObservationInfo(
			ContinuityOfCareDocument continuityOfCareDocument,
			Observation observation, String person_id, String visit_occurence_id) {
		try {
			String observation_id = "0"; // observation.getIds().get(0).getExtension();
			// person_id provided as the foreign key
			String observation_concept_id = "0"; // observation.getTemplateIds().get(0).getRoot();
			String observation_date = "2014-02-06 10:10:10"; // observation.getEffectiveTime().getLow().getValue().substring(0,
																// 8);
			String observation_time = "2014-02-06 10:10:10";
			String value_as_number = "0"; // observation.getValues().get(0).toString();
			String value_as_string = "0"; // observation.getValues().get(0).toString();
			String value_as_concept_id = "0"; // observation.getValues().get(0).toString();
			String unit_concept_id = "";

			String valueToOutput = observation_id + "\01" + person_id + "\01"
					+ observation_concept_id + "\01" + observation_date + "\01"
					+ observation_time + "\01" + value_as_number + "\01"
					+ value_as_string + "\01" + value_as_concept_id + "\01"
					+ unit_concept_id + "\01" + 0 + "\01" + 0 + "\01" + 0
					+ "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0;
			multipleOutputs.write("observation", new Text(valueToOutput), null);
		} catch (Throwable t) {
			LOG.error("Unable to parse content from XML file", t);
		}
	}

	public static String getObservationPeriodInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + "0" + "\01" + "2014-02-05 10:10:10" + "\01"
				+ "2014-02-05 10:10:10";
	}

	public static String getOrganizationInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0;
	}

	public static String getPayerPlanPeriodInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + 0 + "\01" + "2014-02-06 10:10:10" + "\01"
				+ "2014-02-06 10:10:10" + "\01" + 0 + "\01" + 0 + "\01" + 0;
	}

	public String proceedPersonInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		String recordLine = "";
		try {
			EList<PatientRole> patients = continuityOfCareDocument
					.getPatientRoles();
			if ((patients != null) && (patients.size() > 0)) {
				for (PatientRole patient : patients) {
					String birthDate = patient.getPatient().getBirthTime()
							.getValue();

					String person_id = patient.getIds().get(0).getExtension();
					while (person_id.indexOf('-') >= 0)
						person_id = person_id.replace('-', '0');
					person_id = "" + Math.round(Math.random() * 10000000);
					String gender_concept_id = "0"; // getCode(patient.getPatient().getAdministrativeGenderCode());
					String year_of_birth = birthDate.substring(0, 4);
					String month_of_birth = birthDate.substring(4, 5);
					String day_of_birth = birthDate.substring(6, 7);
					String race_concept_id = getCode(patient.getPatient()
							.getRaceCode());
					String ethnicity_concept_id = getCode(patient.getPatient()
							.getEthnicGroupCode());
					String location_id = "0";
					String provider_id = "0"; // patient.getProviderOrganization().getIds().get(0).getExtension();
					String care_site = "0"; // patient.getProviderOrganization().getNames().get(0).getText();
					String person_source_value = "0"; // patient.getPatient().getNames().get(0).getText();
					String gender_source_value = "0"; // patient.getPatient().getAdministrativeGenderCode().getDisplayName();
					String race_source_value = "0";
					String ethnicity_source_value = "0";

					//
					if ((!NumberUtils.isNumber(person_id))
							|| (!NumberUtils.isNumber(gender_concept_id)) || (!NumberUtils.isNumber(year_of_birth)) || (!NumberUtils.isNumber(month_of_birth))
							|| (!NumberUtils.isNumber(day_of_birth)) || (!NumberUtils.isNumber(race_concept_id)) || (!NumberUtils.isNumber(ethnicity_concept_id))) {
						throw new Exception("Number format Exception");
					}
					//
					
					LOG.info("ethnicity_concept_id=" + Long.parseLong(ethnicity_concept_id));

					recordLine = person_id + "\01" + gender_concept_id + "\01"
							+ year_of_birth + "\01" + month_of_birth + "\01"
							+ day_of_birth + "\01" + race_concept_id + "\01"
							+ ethnicity_concept_id + "\01" + location_id
							+ "\01" + provider_id + "\01" + care_site + "\01"
							+ person_source_value + "\01" + gender_source_value
							+ "\01" + race_source_value + "\01"
							+ ethnicity_source_value;

					// Call subsequent tables to be fed.
					// processVisitOccurence(continuityOfCareDocument,
					// person_id);
					processVisitOccurence(continuityOfCareDocument, "0");
				}
			}
		} catch (Throwable t) {
			LOG.error("Unable to parse content from XML file", t);
		}

		return recordLine;

	}

	public static String getProcedureCostInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0
				+ "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0
				+ "\01" + 0 + "\01" + 0 + "\01" + 0;
	}

	public static String getProcedureOccurenceInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		ProceduresSection procedureSection = continuityOfCareDocument
				.getProceduresSection();
		String procedure_occurence_id = "0"; // procedureSection.getId()!=null?procedureSection.getId().getExtension():"0";
		String person_id = "0";
		String procedure_concept_id = "0";
		String procedure_date = "2014-02-06 10:10:10";
		String procedutre_type_concept_id = "0"; // procedureSection.getTypeId()!=null?procedureSection.getTypeId().getExtension():"0";
		String associated_provider_id = "0";
		String visit_occurence_id = "0";
		String relevant_condition_concept_id = "0";
		String procedure_source_value = "0";
		return procedure_occurence_id + "\01" + person_id + "\01"
				+ procedure_concept_id + "\01" + procedure_date + "\01"
				+ procedutre_type_concept_id + "\01" + associated_provider_id
				+ "\01" + visit_occurence_id + "\01"
				+ relevant_condition_concept_id + "\01"
				+ procedure_source_value;
	}

	public static String getProviderInfo(
			ContinuityOfCareDocument continuityOfCareDocument) {
		return 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0 + "\01" + 0
				+ "\01" + 0;
	}

	public void processVisitOccurence(
			ContinuityOfCareDocument continuityOfCareDocument, String person_id) {
		try {
			String visit_occurence_id = continuityOfCareDocument.getId()
					.getExtension();
			while (visit_occurence_id.indexOf('-') >= 0)
				visit_occurence_id = visit_occurence_id.replace('-', '0');
			visit_occurence_id = "" + Math.round(Math.random() * 10000000);
			// person_id provided as the foreign key
			String visit_start_date = "2014-02-06 10:10:10"; // continuityOfCareDocument.getDocumentationOfs().get(0).getServiceEvent().getEffectiveTime().getLow().getValue().substring(0,
																// 8);
			String visit_end_date = "2014-02-06 10:10:10"; // continuityOfCareDocument.getDocumentationOfs().get(0).getServiceEvent().getEffectiveTime().getLow().getValue().substring(0,
															// 8);
			String place_of_service_concept_id = "0"; // continuityOfCareDocument.getDocumentationOfs().get(0).getServiceEvent().getPerformers().get(0).getAssignedEntity().getIds().get(0).getExtension();
			String care_site_id = "0"; // continuityOfCareDocument.getAuthors().get(0).getAssignedAuthor().getRepresentedOrganization().getIds().get(0).getExtension();
			String place_of_service_source_value = continuityOfCareDocument
					.getAuthors().get(0).getAssignedAuthor().getAddrs().get(0)
					.getStreetAddressLines().get(0).getText();

			String valueToOutput = visit_occurence_id + "\01" + person_id
					+ "\01" + visit_start_date + "\01" + visit_end_date + "\01"
					+ place_of_service_concept_id + "\01" + care_site_id
					+ "\01" + place_of_service_source_value;
			multipleOutputs.write("visitoccurrence", new Text(valueToOutput),
					null);

			for (Section section : continuityOfCareDocument.getAllSections())
				if (section.getObservations() != null)
					for (Observation observation : section.getObservations())
						proceedObservationInfo(continuityOfCareDocument,
								observation, person_id, visit_occurence_id);
		} catch (Throwable t) {
			LOG.error("Unable to parse content from XML file", t);
		}
	}

	/**
	 * Map the entire into into a single sequence in the output, using the input
	 * file name as the key
	 */
	@Override
	protected void map(Text key, Text value, Context context)
			throws IOException, InterruptedException {
		LOG.info("Working on " + key);
		String stringValue = new String(value.copyBytes());

		InputStream is = new ByteArrayInputStream(stringValue.getBytes());
		ContinuityOfCareDocument continuityOfCareDocument;
		try {
			continuityOfCareDocument = getDocumentFromStream(is);
			validateDocument(continuityOfCareDocument);
			if (continuityOfCareDocument == null) {
				context.getCounter(MATCH_COUNTER.FAILED_DOCUMENT).increment(1);
				return;
			} else
				context.getCounter(MATCH_COUNTER.DOCUMENT).increment(1);
			EList<CS> realms = continuityOfCareDocument.getRealmCodes();
			boolean isUS = false;
			for (CS realm : realms) {
				if ("US".equals(realm.getCode()))
					isUS = true;
			}
			if (!isUS)
				return;

			String valueFromXML = proceedPersonInfo(continuityOfCareDocument);

			if (valueFromXML != null) {
				multipleOutputs.write("person", new Text(valueFromXML), null);
			}

			valueFromXML = getCareSiteInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("caresite", new Text(valueFromXML), null);
			}
			valueFromXML = getCohortInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("cohort", new Text(valueFromXML), null);
			}
			valueFromXML = getConditionEraInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("conditionera", new Text(valueFromXML),
						null);
			}
			valueFromXML = getConditionOccurenceInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("conditionoccurrence", new Text(
						valueFromXML), null);
			}
			valueFromXML = getDeathInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("death", new Text(valueFromXML), null);
			}
			valueFromXML = getDrugCostInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("drugcost", new Text(valueFromXML), null);
			}
			valueFromXML = getDrugEraInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("drugera", new Text(valueFromXML), null);
			}
			valueFromXML = getDrugExposureInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("drugexposure", new Text(valueFromXML),
						null);
			}
			valueFromXML = getLocationInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("location", new Text(valueFromXML), null);
			}
			/*
			 * valueFromXML = getObservationInfo(continuityOfCareDocument); if
			 * (valueFromXML != null) { multipleOutputs.write("observation", new
			 * Text(valueFromXML), null); }
			 */
			valueFromXML = getObservationPeriodInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("observationperiod", new Text(
						valueFromXML), null);
			}
			valueFromXML = getOrganizationInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("organization", new Text(valueFromXML),
						null);
			}
			valueFromXML = getPayerPlanPeriodInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("payerplanperiod",
						new Text(valueFromXML), null);
			}
			valueFromXML = getProcedureCostInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("procedurecost", new Text(valueFromXML),
						null);
			}
			valueFromXML = getProcedureOccurenceInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("procedureoccurrence", new Text(
						valueFromXML), null);
			}
			valueFromXML = getProviderInfo(continuityOfCareDocument);
			if (valueFromXML != null) {
				multipleOutputs.write("provider", new Text(valueFromXML), null);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		context.progress();
	}

	@Override
	protected void cleanup(Context context) throws IOException,
			InterruptedException {
		multipleOutputs.close();
	}

	public static void main(String args[]) {
		try {
			ConsolPackage.eINSTANCE.eClass();
			File[] files = new File(
					"/home/jmspaggiari/Documents/Quintiles/xml/").listFiles();
			for (File file : files) {
				FileInputStream fis = new FileInputStream(file);
				ContinuityOfCareDocument continuityOfCareDocument = getDocumentFromStream(fis);
				validateDocument(continuityOfCareDocument);
				if (file.getName().indexOf(
						"Patient_1497027845000500_1390223025280.xml") >= 0) {
					System.out.println("Processing " + file.getPath());
					System.out.println(continuityOfCareDocument
							.getDocumentationOfs().get(0).getServiceEvent()
							.getEffectiveTime().getLow().getValue());
					System.out.println(continuityOfCareDocument
							.getDocumentationOfs().get(0).getServiceEvent()
							.getEffectiveTime().getHigh().getValue());
					for (Section section : continuityOfCareDocument
							.getAllSections()) {
						// System.out.println (section.getTitle().getText());
						if (section.getTitle().getText()
								.indexOf("Observations") >= 0) {
							for (Observation observation : section
									.getObservations()) {
								for (ANY any : observation.getValues()) {
									if (any instanceof STImpl) {
										for (EObject object : any.eContainer()
												.eContents()) {
											if (object instanceof CDImpl) {
												CDImpl impl = (CDImpl) object;
												System.out
														.println("2 => "
																+ impl.getOriginalText()
																		.getText());
											} else if (object instanceof IVL_TSImpl) {
												IVL_TSImpl impl = (IVL_TSImpl) object;
												System.out.println("3 => "
														+ impl.getLow()
																.getValue());
												System.out.println("3 => "
														+ impl.getHigh()
																.getValue());
											}
										}
									}
								}
							}
						} else
							System.out.println(section.getTitle().getText());
					}
				}
				// System.out.println (getPersonInfo(continuityOfCareDocument));
				// System.out.println ((System.currentTimeMillis() - time));
				// time = System.currentTimeMillis();
				// System.out.println
				// (validateDocument(continuityOfCareDocument));
				// System.out.println ((System.currentTimeMillis() - time));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
