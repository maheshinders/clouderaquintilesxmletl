package com.cloudera.sa.quintiles.mapreduce.draft;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.text.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.conf.*;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import org.openhealthtools.mdht.uml.cda.consol.*;
import org.openhealthtools.mdht.uml.cda.*;
import org.openhealthtools.mdht.uml.cda.util.*;
import org.openhealthtools.mdht.uml.hl7.datatypes.*;
import org.eclipse.emf.ecore.xml.type.*;
import org.eclipse.emf.ecore.*;
import org.eclipse.emf.ecore.util.*;

public class Demo_Mdht_1_2_0 {

  //
  private static Logger log = Logger.getLogger(Demo_Mdht_1_2_0.class.getName());
  //
  private static SimpleDateFormat sdfIn1 = new SimpleDateFormat("yyyyMMdd"),
      sdfIn2 = new SimpleDateFormat("yyyyMMddhhmmssZZZZZ"), sdfOut1 = new SimpleDateFormat(
          "yyyy-MM-dd"), sdfOut2 = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS"),
      sdfOut3 = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS z"),
      sdfFileSuffix = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_z");
  //
  private static final String fieldDelimiter = "\t", recordDelimiter = "\n";
  //
  private static Properties propReader = new Properties();
  //
  private static String propFilename = "application.properties", inputPath = "", outputPathId = "";
  //
  private static boolean useHDFSForInputData;
  //
  private static FileSystem fileSystem = null;
  //
  private static BufferedWriter headerWriter = null, problemSectionWriter = null;
  //
  private static final String headerName = "header", problemSectionName = "problems";
  //
  private static final short totalHeaderFields = 5, totalProblemSectionFields = 8;

  // main method
  public static void main(String[] args) {

    //
    Deque<String> stack = new ArrayDeque<String>();
    String[] FileExtensions = null, Patterns = null;
    String propValue;

    // main try
    try {

      //
      if (args.length > 0) {
        propFilename = args[0];
      }

      //
      PropertyConfigurator.configure("log4j.properties");

      // load props
      java.net.URL urlPropFile = Demo_Mdht_1_2_0.class.getClassLoader().getResource(propFilename);
      propReader.load(urlPropFile.openStream());

      // hadoop configuration
      Configuration conf = new Configuration();
      fileSystem = FileSystem.get(conf);

      //
      if ((propValue = (String) propReader.get("UseHDFSForInputData")) == null) {
        throw new RuntimeException("Missing property = UseHDFSForInputData");
      }
      try {
        useHDFSForInputData = Integer.parseInt(propValue) == 1;
      } catch (Exception e) {
      }

      // Implicit initialization
      ConsolPackage.eINSTANCE.eClass();

      //
      openOutputFiles();

      //
      if (args.length > 1) {
        inputPath = args[1];
      } else {
        if ((propValue = (String) propReader.get("InputDir")) == null) {
          throw new RuntimeException("Missing property = InputDir");
        } else {
          inputPath = propValue;
        }
      }
      if (inputPath.length() == 0) {
        throw new RuntimeException("Missing Input Path");
      }

      //
      if (useHDFSForInputData) {

        FileStatus[] fileStatus = fileSystem.listStatus(new Path(inputPath));
        for (FileStatus fileStat : fileStatus) {
          //
          log.info("Processing:" + inputPath + File.separator + fileStat.getPath().getName());
          try {
            processDocument(inputPath + File.separator + fileStat.getPath().getName());
          } catch (Exception e) {
            log.error("Error", e);
          }
        }

      } else {

        // set up filter for dir(s)
        FileFilter dirFilter = new FileFilter() {
          public boolean accept(File file) {
            return file.isDirectory();
          }
        };

        // set up filter for file(s)
        FileFilter fileFilter = new FileFilter() {
          public boolean accept(File file) {
            return file.isFile();
          }
        };

        // process all dir(s)
        stack.addFirst(inputPath);
        while (stack.size() > 0) {

          //
          String DirName = stack.removeFirst();

          //
          try {
            ListDirs(stack, dirFilter, DirName);
          } catch (Exception e) {
            continue;
          }

          // get a list of files(s) under the dir
          File dir = new File(DirName);
          //
          if (dir == null) {
            continue;
          }
          //
          File[] files = dir.listFiles(fileFilter);

          //
          if (files == null) {
            continue;
          }

          // iterate all file(s)
          for (int i = 0, j = files.length; i < j; i++) {
            //
            log.info("Processing:" + DirName + File.separator + files[i].getName());
            try {
              processDocument(DirName + File.separator + files[i].getName());
            } catch (Exception e) {
              log.error("Error", e);
            }

          } // iterate all file(s)

        } // process all dir(s)

      }

      //
      closeOutputFiles();
      //
      createImpalaTables();

    } catch (Exception e) {
      log.error("Error", e);
    } finally {
      //
      closeOutputFiles();
    }

  } // main method

  // open output file(s)
  public static void openOutputFiles() throws Exception, RuntimeException {

    //
    String propValue;

    //
    if ((propValue = (String) propReader.get("OutputDir")) == null) {
      throw new RuntimeException("Missing property = OutputDir");
    }
    //
    outputPathId = sdfFileSuffix.format(new java.util.Date());
    //
    Path path =
        new Path(propValue + File.separator + outputPathId + File.separator + headerName
            + File.separator + "data.txt");
    headerWriter = new BufferedWriter(new OutputStreamWriter(fileSystem.create(path, true)));
    path =
        new Path(propValue + File.separator + outputPathId + File.separator + problemSectionName
            + File.separator + "data.txt");
    problemSectionWriter =
        new BufferedWriter(new OutputStreamWriter(fileSystem.create(path, true)));

  }

  // close output file(s)
  public static void closeOutputFiles() {

    // close file(s)
    try {
      if (headerWriter != null) {
        headerWriter.close();
        headerWriter = null;
      }
      if (problemSectionWriter != null) {
        problemSectionWriter.close();
        problemSectionWriter = null;
      }
    } catch (Exception e) {
      log.error("Error", e);
    }

  } //

  // create Impala table(s)
  public static void createImpalaTables() throws Exception {

    String propValue, outputDir, tablePrefix;
    //
    if ((propValue = (String) propReader.get("OutputDir")) == null) {
      throw new RuntimeException("Missing property = OutputDir");
    }
    outputDir = propValue;
    if ((propValue = (String) propReader.get("HeaderTablePrefix")) == null) {
      throw new RuntimeException("Missing property = HeaderTablePrefix");
    }
    tablePrefix = propValue;
    //
    createImpalaTable(tablePrefix + outputPathId, outputDir + File.separator + outputPathId
        + File.separator + headerName, "(patient_id string," + "first_name string,"
        + "middle_name string," + "last_name string, dob timestamp)");
    //
    if ((propValue = (String) propReader.get("ProblemSectionTablePrefix")) == null) {
      throw new RuntimeException("Missing property = ProblemSectionTablePrefix");
    }
    tablePrefix = propValue;
    createImpalaTable(tablePrefix + outputPathId, outputDir + File.separator + outputPathId
        + File.separator + problemSectionName, "(patient_id string," + "code_display_name string,"
        + "status_code string," + "diagnosis_display_name string,"
        + "diagnosis_observation_text_reference string,"
        + "diagnosis_observation_status_code_code string,"
        + "diagnosis_observation_effective_time_low string,"
        + "diagnosis_observation_effective_time_high string)");

  } //

  // process document
  public static void processDocument(String fileName) throws Exception, RuntimeException {

    //
    ValidationResult result = new ValidationResult();
    String patientId = "";

    ContinuityOfCareDocument continuityOfCareDocument = null;
    //
    if (useHDFSForInputData) {
      //
      Path path = new Path(fileName);
      FSDataInputStream dins = fileSystem.open(path);
      continuityOfCareDocument = (ContinuityOfCareDocument) CDAUtil.load(dins, result);
    } else {
      //
      continuityOfCareDocument =
          (ContinuityOfCareDocument) CDAUtil.load(new FileInputStream(fileName));
    }

    // validation on clinical document object
    // CDAUtil.validate(continuityOfCareDocument, new BasicValidationHandler() {
    // public void handleError(Diagnostic diagnostic) {
    // //log.error(diagnostic.getMessage());
    // }
    // public void handleWarning(Diagnostic diagnostic) {
    // //log.info(diagnostic.getMessage());
    // }
    // });

    // process Header
    HashMap<String, HashMap<String, String>> headerRecord = processHeader(continuityOfCareDocument);
    dumpHashMap(headerRecord);
    // process Problem section
    HashMap<String, HashMap<String, String>> problemSectionRecords =
        processProblemSection(continuityOfCareDocument);
    dumpHashMap(problemSectionRecords);
    //
    patientId = writeHeader(headerRecord);
    //
    if (patientId.length() == 0) {
      throw new Exception("Missing Patient Id");
    }
    //
    writeProblemSection(patientId, problemSectionRecords);

  } //

  //
  public static String writeHeader(HashMap<String, HashMap<String, String>> docRecords)
      throws Exception {

    //
    String propValue, recordValue, patientId = "";
    String[] headerFields = new String[totalHeaderFields];

    //
    HashMap<String, String> headerMap = docRecords.get("Header");

    //
    if (headerMap == null) {
      throw new Exception("Missing Header Section");
    }

    //
    initArray(headerFields);

    if ((propValue = headerMap.get("recordTarget.patientRole.id.1")) != null) {
      patientId = propValue;
      headerFields[0] = patientId;
    } else {
      throw new Exception("Missing field:Patient Id");
    }
    if ((propValue = headerMap.get("recordTarget.patientRole.patient.name.given.1")) != null) {
      headerFields[1] = propValue;
    } else {
      throw new Exception("Missing field:Patient First Name");
    }
    if ((propValue = headerMap.get("recordTarget.patientRole.patient.name.given.2")) != null) {
      headerFields[2] = propValue;
    } else {
      headerFields[2] = "";
    }
    if ((propValue = headerMap.get("recordTarget.patientRole.patient.name.family.1")) != null) {
      headerFields[3] = propValue;
    } else {
      throw new Exception("Missing field:Patient Family Name");
    }
    if ((propValue = headerMap.get("recordTarget.patientRole.patient.birthTime")) != null) {
      headerFields[4] = propValue;
    } else {
      throw new Exception("Missing field:Patient Birth Time");
    }
    //
    recordValue = "";
    for (short si = 0; si < totalHeaderFields; si++) {
      recordValue += ((si == 0) ? "" : fieldDelimiter) + headerFields[si];
    }
    //
    recordValue += recordDelimiter;
    headerWriter.write(recordValue);

    //
    return patientId;

  } //

  //
  public static void writeProblemSection(String patientId,
      HashMap<String, HashMap<String, String>> docRecords) throws Exception {

    //
    String recordValue;
    String[] problemSectionFields = new String[totalProblemSectionFields];

    //
    Set<Map.Entry<String, HashMap<String, String>>> set1 = docRecords.entrySet();
    Iterator<Map.Entry<String, HashMap<String, String>>> it1 = set1.iterator();
    while (it1.hasNext()) {
      //
      Map.Entry<String, HashMap<String, String>> pairs1 = it1.next();
      //
      Set<Map.Entry<String, String>> set2 = pairs1.getValue().entrySet();
      Iterator<Map.Entry<String, String>> it2 = set2.iterator();
      //
      initArray(problemSectionFields);
      problemSectionFields[0] = patientId;
      while (it2.hasNext()) {
        //
        Map.Entry<String, String> pairs2 = it2.next();
        //
        if (pairs2.getKey().matches("entry.act.[0-9]*.code.displayName")) {
          problemSectionFields[1] = pairs2.getValue();
        }
        if (pairs2.getKey().matches("entry.act.[0-9]*.statusCode.code")) {
          problemSectionFields[2] = pairs2.getValue();
        }
        if (pairs2.getKey().matches(
          "entry.act.[0-9]*.entryRelationship.[0-9]*.observation\\(Diagnosis\\).code.displayName")) {
          problemSectionFields[3] = pairs2.getValue();
        }
        if (pairs2.getKey().matches(
          "entry.act.[0-9]*.entryRelationship.[0-9]*.observation\\(Diagnosis\\).text.reference")) {
          problemSectionFields[4] = pairs2.getValue();
        }
        if (pairs2.getKey().matches(
          "entry.act.[0-9]*.entryRelationship.[0-9]*.observation\\(Diagnosis\\).statusCode.code")) {
          problemSectionFields[5] = pairs2.getValue();
        }
        if (pairs2.getKey().matches(
          "entry.act.[0-9]*.entryRelationship.[0-9]*.observation\\(Diagnosis\\).effectiveTime.low")) {
          problemSectionFields[6] = pairs2.getValue();
        }
        if (pairs2
            .getKey()
            .matches(
              "entry.act.[0-9]*.entryRelationship.[0-9]*.observation\\(Diagnosis\\).effectiveTime.high")) {
          problemSectionFields[7] = pairs2.getValue();
        }
        //
        it2.remove();
      }
      //
      recordValue = "";
      for (short si = 0; si < totalProblemSectionFields; si++) {
        recordValue += ((si == 0) ? "" : fieldDelimiter) + problemSectionFields[si];
      }
      //
      recordValue += recordDelimiter;
      problemSectionWriter.write(recordValue);
      //
      it1.remove();
    }

  } //

  //
  public static HashMap<String, HashMap<String, String>> processHeader(
      ContinuityOfCareDocument continuityOfCareDocument) throws Exception {

    //
    HashMap<String, HashMap<String, String>> docRecords =
        new HashMap<String, HashMap<String, String>>();
    HashMap<String, String> hashMap = new HashMap<String, String>();
    short si = 1;

    //
    if (continuityOfCareDocument.getRecordTargets().size() == 0) {
      throw new Exception("Missing Header section");
    }

    //
    for (org.openhealthtools.mdht.uml.cda.RecordTarget recordTarget : continuityOfCareDocument
        .getRecordTargets()) {
      //
      org.openhealthtools.mdht.uml.cda.PatientRole patientRole = recordTarget.getPatientRole();

      //
      si = 1;
      for (II ii : patientRole.getIds()) {
        hashMap.put("recordTarget.patientRole.id." + si++, ii.getRoot() + "." + ii.getExtension());
      }
      //
      org.openhealthtools.mdht.uml.cda.Patient patient = patientRole.getPatient();
      for (EN en : patient.getNames()) {
        si = 1;
        for (ED ed : en.getGivens()) {
          hashMap.put("recordTarget.patientRole.patient.name.given." + si++, ed.getText());
        }
        si = 1;
        for (ED ed : en.getFamilies()) {
          hashMap.put("recordTarget.patientRole.patient.name.family." + si++, ed.getText());
        }
        //
        hashMap.put("recordTarget.patientRole.patient.birthTime",
          sdfOut1.format(sdfIn1.parse(patient.getBirthTime().getValue())));
        break;
      }

      //
      docRecords.put("Header", hashMap);

      //
      break;

    }

    //
    return docRecords;

  }

  //
  public static HashMap<String, HashMap<String, String>> processProblemSection(
      ContinuityOfCareDocument continuityOfCareDocument) throws Exception {

    //
    HashMap<String, HashMap<String, String>> docRecords =
        new HashMap<String, HashMap<String, String>>();
    HashMap<String, String> hashMap = new HashMap<String, String>();
    short si = 0;

    //
    ProblemSection problemSection = continuityOfCareDocument.getProblemSection();

    //
    if (problemSection == null) {
      throw new Exception("Missing Problem section");
    }

    //
    List<HashMap<String, String>> attributeMapList = new ArrayList<HashMap<String, String>>();
    StrucDocText strucDocText = problemSection.getText();
    HashMap<String, String> allAttributesMap =
        TraverseFeatureMap(attributeMapList, strucDocText.getMixed());

    //
    si = 1;
    for (II ii : problemSection.getTemplateIds()) {
      hashMap.put("ProblemSection.templateId.root." + si++, ii.getRoot());
    }

    //
    CD cd = problemSection.getCode();
    if (cd != null) {
      hashMap.put("ProblemSection.code.code", cd.getCode());
      hashMap.put("ProblemSection.code.codeSystem", cd.getCodeSystem());
      hashMap.put("ProblemSection.code.codeSystemName", cd.getCodeSystemName());
      hashMap.put("ProblemSection.code.displayName", cd.getDisplayName());
    }

    //
    ED ed = problemSection.getTitle();
    if (ed != null) {
      hashMap.put("ProblemSection.title", ed.getText());
    }

    //
    docRecords.put("ProblemSection", hashMap);

    //
    processProblemSectionEntries(allAttributesMap, problemSection, docRecords);

    //
    allAttributesMap.clear();

    //
    return docRecords;

  }

  //
  public static void processProblemSectionEntries(HashMap<String, String> allAttributesMap,
      ProblemSection section, HashMap<String, HashMap<String, String>> docRecords) throws Exception {

    //
    short si = 0, entryNum = 1, entryRelationshipNum = 1;

    List<Entry> allEntries = section.getEntries();

    //
    if ((allEntries == null) || (allEntries.size() == 0)) {
      throw new Exception("Missing Entries in Problem Section");
    }

    // all entries
    for (Entry entry : allEntries) {

      //
      HashMap<String, String> hashMap = new HashMap<String, String>();

      //
      hashMap.put("entry.act." + entryNum + ".typeCode", entry.getTypeCode().toString());

      //
      Act act = entry.getAct();
      if (act == null) {
        continue;
      }
      //
      if (act.getClassCode() != null) {
        hashMap.put("entry.act." + entryNum + ".classCode", act.getClassCode().toString());
      }
      //
      if (act.getMoodCode() != null) {
        hashMap.put("entry.act." + entryNum + ".moodCode", act.getMoodCode().toString());
      }
      //
      si = 1;
      for (II ii : act.getTemplateIds()) {
        hashMap.put("entry.act." + entryNum + ".templateId.root." + si++, ii.getRoot());
      }
      //
      si = 1;
      for (II ii : act.getIds()) {
        hashMap.put("entry.act." + entryNum + ".id.root_" + si++, ii.getRoot());
        hashMap.put("entry.act." + entryNum + ".id.extension_" + si++, ii.getExtension());
      }
      //
      CD cd = act.getCode();
      if (cd != null) {
        hashMap.put("entry.act." + entryNum + ".code.code", cd.getCode());
        hashMap.put("entry.act." + entryNum + ".code.codeSystem", cd.getCodeSystem());
        hashMap.put("entry.act." + entryNum + ".code.displayName", cd.getDisplayName());
      }
      //
      CS cs = act.getStatusCode();
      if (cs != null) {
        hashMap.put("entry.act." + entryNum + ".statusCode.code", cs.getCode());
      }
      //
      if (act.getEffectiveTime() != null) {
        //
        extractEffectiveTime("entry.act." + entryNum, act.getEffectiveTime(), hashMap);
      }

      //
      HashMap<String, String> hashMapCopy = hashMap;
      entryRelationshipNum = 1;

      // all EntryRelationship
      for (EntryRelationship entryRelationship : act.getEntryRelationships()) {

        //
        processEntryRelationships(entryNum, entryRelationship, entryRelationshipNum,
          allAttributesMap, hashMap);
        //
        docRecords.put("ProblemSection.entry." + entryNum + ".entryRelationship."
            + entryRelationshipNum, hashMap);
        //
        hashMap = hashMapCopy;

        //
        entryRelationshipNum++;

      } // all EntryRelationship

      //
      entryNum++;

    } // all entries

  } //

  //
  public static void processEntryRelationships(short entryNum, EntryRelationship entryRelationship,
      int entryRelationshipNum, HashMap<String, String> allAttributesMap,
      HashMap<String, String> hashMap) throws Exception {

    //
    Observation observation = entryRelationship.getObservation();
    //
    if (observation == null) {
      throw new Exception("Missing Observation in Entry element of Problem Section");
    }
    //
    hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
        + ".typeCode", entryRelationship.getTypeCode().toString());
    //
    if (entryRelationship.getInversionInd() != null) {
      hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
          + ".inversionInd", entryRelationship.getInversionInd() ? "T" : "F");
    }

    //
    String observationType =
        processObservation(entryNum, entryRelationshipNum, allAttributesMap, hashMap, "",
          observation);

    // all EntryRelationship
    for (EntryRelationship entryRelationship1 : observation.getEntryRelationships()) {
      //
      Observation observation1 = entryRelationship1.getObservation();
      //
      if (observation1 != null) {
        processObservation(entryNum, entryRelationshipNum, allAttributesMap, hashMap,
          observationType, observation1);
      }
    } // all EntryRelationship

  } //

  //
  public static String processObservation(short entryNum, int entryRelationshipNum,
      HashMap<String, String> allAttributesMap, HashMap<String, String> hashMap,
      String parentObservationType, Observation observation) throws Exception {

    //
    short si;
    String observationType = "", subObservationType = "";

    //
    CD cd = observation.getCode();
    if (cd == null) {
      throw new Exception("Missing CD in Entry->Observation element of Problem Section");
    }

    //
    observationType = cd.getDisplayName();
    //
    if (cd.getNullFlavor() != null) {
      hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
          + ".observation(" + parentObservationType + observationType + ").code.nullFlavor", cd
          .getNullFlavor().toString());
    }
    hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
        + ".observation(" + parentObservationType + observationType + ").code.codeSystem",
      cd.getCodeSystem());
    hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
        + ".observation(" + parentObservationType + observationType + ").code.codeSystemName",
      cd.getCodeSystemName());
    hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
        + ".observation(" + parentObservationType + observationType + ").code.displayName",
      cd.getDisplayName());

    //
    if (observation.getNullFlavor() != null) {
      hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
          + ".observation(" + parentObservationType + observationType + ").nullFlavor", observation
          .getNullFlavor().toString());
    }
    //
    if (observation.getClassCode() != null) {
      hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
          + ".observation(" + parentObservationType + observationType + ").classCode", observation
          .getClassCode().toString());
    }
    //
    if (observation.getMoodCode() != null) {
      hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
          + ".observation(" + parentObservationType + observationType + ").moodCode", observation
          .getMoodCode().toString());
    }
    //
    si = 1;
    for (II ii : observation.getTemplateIds()) {
      hashMap.put(
        "entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum + ".observation("
            + parentObservationType + observationType + ").templateId.root." + si++, ii.getRoot());
    }
    //
    si = 1;
    for (II ii : observation.getIds()) {
      hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
          + ".observation(" + parentObservationType + observationType + ").id.root." + si++,
        ii.getRoot());
      hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
          + ".observation(" + parentObservationType + observationType + ").id.extension." + si++,
        ii.getRoot());
    }
    //
    ED ed = observation.getText();
    if (ed != null) {
      TEL tel = ed.getReference();
      if (tel != null) {
        hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
            + ".observation(" + parentObservationType + observationType + ").text.reference",
          allAttributesMap.get(tel.getValue().replaceAll("^#", "")));
      }
    }
    //
    CS cs = observation.getStatusCode();
    if (cs != null) {
      hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
          + ".observation(" + parentObservationType + observationType + ").statusCode.code",
        cs.getCode());
    }
    //
    if (observation.getEffectiveTime() != null) {
      //
      extractEffectiveTime("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
          + ".observation(" + parentObservationType + observationType + ")",
        observation.getEffectiveTime(), hashMap);
    }
    //
    List<ANY> values = observation.getValues();
    if (values != null) {
      processValues("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
          + ".observation(" + parentObservationType + observationType + ")", values, hashMap,
        allAttributesMap);
    }
    //
    for (org.openhealthtools.mdht.uml.cda.Author author : observation.getAuthors()) {
      //
      TS ts = author.getTime();
      hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
          + ".observation(" + parentObservationType + observationType + ").author.time.value",
        ts.getValue());
      org.openhealthtools.mdht.uml.cda.AssignedAuthor assignedAuthor = author.getAssignedAuthor();
      si = 1;
      for (II ii : assignedAuthor.getIds()) {
        if (ii.getNullFlavor() != null) {
          hashMap.put("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
              + ".observation(" + parentObservationType + observationType
              + ").author.assignedAuthor." + si++ + ".nullFlavor", ii.getNullFlavor().toString());
        }
      }
    }

    //
    si = 1;
    for (EntryRelationship entryRelationship1 : observation.getEntryRelationships()) {
      Observation observation1 = entryRelationship1.getObservation();
      if ((cd = observation1.getCode()) == null) {
        throw new Exception("Missing CD in Entry->Observation element of Problem Section");
      }
      //
      subObservationType = "." + cd.getDisplayName();
      if ((values = observation1.getValues()) != null) {
        processValues("entry.act." + entryNum + ".entryRelationship." + entryRelationshipNum
            + ".observation(" + parentObservationType + observationType + subObservationType + ")",
          values, hashMap, allAttributesMap);
      }
    }

    //
    return parentObservationType + ((parentObservationType.length() > 0) ? "." : "")
        + observationType + ".";

  } //

  //
  public static void processValues(String keyName, List<ANY> values,
      HashMap<String, String> hashMap, HashMap<String, String> allAttributesMap) {

    short si = 1;
    CD cd;
    ED ed;
    PQ pq;

    //
    for (int i1 = 0; i1 < values.size(); i1++) {
      //
      if (values.get(i1) instanceof CD) {
        cd = (CD) values.get(i1);
        si = 1;
        if (cd.getNullFlavor() != null) {
          hashMap.put(keyName + ".value." + si + ".null_flavor", cd.getNullFlavor().toString());
        }
        hashMap.put(keyName + ".value." + si + ".code", cd.getCode());
        hashMap.put(keyName + ".value." + si + ".codeSystem", cd.getCodeSystem());
        hashMap.put(keyName + ".value." + si + ".codeSystemName", cd.getCodeSystemName());
        hashMap.put(keyName + ".value." + si + ".displayName", cd.getDisplayName());
        //
        ed = cd.getOriginalText();
        if (ed != null) {
          TEL tel = ed.getReference();
          if (tel != null) {
            hashMap.put(keyName + ".value." + si + ".originalText",
              allAttributesMap.get(tel.getValue().replaceAll("^#", "")));
          }
        }
      } else if (values.get(i1) instanceof PQ) {
        pq = (PQ) values.get(i1);
        hashMap.put(keyName + ".value." + si + ".value", pq.getValue().toString());
        hashMap.put(keyName + ".value." + si + ".unit", pq.getUnit());
      }
    }

  } //

  //
  public static void extractEffectiveTime(String keyName, IVL_TS effTime,
      HashMap<String, String> hashMap) {

    //
    String dateString = "";
    if (effTime.getLow() != null) {
      //
      if (effTime.getLow().getValue() != null) {
        try {
          java.util.Date effectiveTime = sdfIn2.parse(effTime.getLow().getValue());
          dateString = sdfOut3.format(effectiveTime);
        } catch (Exception e) {
        }
      } else if (effTime.getLow().getNullFlavor() != null) {
        dateString = effTime.getHigh().getNullFlavor().toString();
      }
    }
    //
    hashMap.put(keyName + ".effectiveTime.low", dateString);
    dateString = "";
    if (effTime.getHigh() != null) {
      //
      if (effTime.getHigh().getValue() != null) {
        try {
          java.util.Date effectiveTime = sdfIn2.parse(effTime.getHigh().getValue());
          dateString = sdfOut3.format(effectiveTime);
        } catch (Exception e) {
        }
      } else if (effTime.getHigh().getNullFlavor() != null) {
        dateString = effTime.getHigh().getNullFlavor().toString();
      }
    }

    //
    hashMap.put(keyName + ".effectiveTime.high", dateString);

  }

  //
  private static HashMap<String, String> TraverseFeatureMap(
      List<HashMap<String, String>> attributeMapList, FeatureMap root) {

    //
    Stack<FeatureMap> stack = new Stack<FeatureMap>();
    int attributeMapIndex = 0;
    HashMap<String, String> allAttributesMap = new HashMap<String, String>();
    String MapKey;

    //
    stack.push(root);
    // till EOS
    while (!stack.isEmpty()) {

      //
      FeatureMap featureMap = stack.pop();
      // all entries
      for (int i = featureMap.size() - 1; i >= 0; i--) {

        //
        FeatureMap.Entry entry = featureMap.get(i);

        //
        if (entry.getEStructuralFeature() instanceof EReference) {

          //
          // log.info(entry.getEStructuralFeature().getName() + " {");
          //
          AnyType anyType = (AnyType) entry.getValue();
          //
          TraverseFeatureMapAttributes(attributeMapList, anyType.getAnyAttribute());
          //
          attributeMapIndex++;
          //
          stack.push(anyType.getMixed());

        } else {

          MapKey = "";

          //
          if ((attributeMapIndex > 0) && (attributeMapIndex <= attributeMapList.size())) {
            HashMap<String, String> attributeMap = attributeMapList.get(attributeMapIndex - 1);
            if (attributeMap != null) {
              Set<Map.Entry<String, String>> set = attributeMap.entrySet();
              Iterator<Map.Entry<String, String>> it = set.iterator();

              while (it.hasNext()) {
                Map.Entry<String, String> pairs = it.next();
                // log.info(pairs.getKey() + "=" + pairs.getValue());
                MapKey = pairs.getValue();
              }
            }
          }

          //
          if (entry.getValue() != null) {

            //
            String value = entry.getValue().toString();
            if (value.trim().length() > 0) {
              // log.info(" " + value + " }");
              //
              attributeMapIndex--;
              attributeMapList.remove(attributeMapList.size() - 1);
              allAttributesMap.put(MapKey, value);
            }

          } else {
            //
            // log.info(" }");
            //
            attributeMapIndex--;
            attributeMapList.remove(attributeMapList.size() - 1);
          }

        }

      } // all entries

    } // till EOS

    //
    return allAttributesMap;

  }

  //
  private static void TraverseFeatureMapAttributes(List<HashMap<String, String>> attributeMapList,
      FeatureMap anyAttribute) {

    //
    HashMap<String, String> attributeMap = null;

    //
    for (FeatureMap.Entry entry : anyAttribute) {

      //
      if (attributeMap == null) {
        attributeMap = new HashMap<String, String>();
      }

      //
      // log.info("attr name: " + entry.getEStructuralFeature().getName() + ", attr value: " +
      // entry.getValue().toString());
      //
      attributeMap.put(entry.getEStructuralFeature().getName(), entry.getValue().toString());

    }

    //
    attributeMapList.add(attributeMap);

  }

  //
  public static void createImpalaTable(String tableName, String tableLocation, String fieldNames)
      throws Exception {

    //
    Connection con = null;
    Statement stmt = null;
    ResultSet rs = null;
    String propValue = "", dbName = "", sqlStmt = "", thriftDbUrl, thriftDbUsername, thriftDbPassword;
    long l;

    //
    try {

      // impala db
      if ((propValue = (String) propReader.get("DbName")) == null) {
        throw new Exception("Missing property = dbName");
      }
      dbName = propValue;
      // db connection props
      if ((propValue = (String) propReader.get("ThriftDbUrl")) == null) {
        throw new Exception("Missing property = thriftDbUrl");
      }
      thriftDbUrl = propValue;
      if ((propValue = (String) propReader.get("ThriftDbUsername")) == null) {
        throw new Exception("Missing property = thriftDbUsername");
      }
      thriftDbUsername = propValue;
      if ((propValue = (String) propReader.get("ThriftDbPassword")) == null) {
        throw new Exception("Missing property = thriftDbPassword");
      }
      thriftDbPassword = propValue;

      //
      // open thrift db
      Class.forName("org.apache.hive.jdbc.HiveDriver").newInstance();
      // get connection
      con = DriverManager.getConnection(thriftDbUrl);
      // create statement
      stmt = con.createStatement();

      // create external table
      sqlStmt =
          "create table " + dbName + "." + tableName + " " + fieldNames + " "
              + "row format delimited " + "fields terminated by '\t' "
              + "lines terminated by '\n' " + "stored as textfile " + "location '" + tableLocation
              + "'";
      //
      if ((l = stmt.executeUpdate(sqlStmt)) < 0) {
        throw new Exception("Cannot create table :" + dbName + "." + tableName);
      }
      //
      log.info(sqlStmt);

    } catch (SQLException e) {
      throw new Exception(e);
    } catch (Exception e) {
      throw new Exception(e);
    } finally {
      //
      try {
        if (rs != null) {
          rs.close();
        }
      } catch (Exception exception) {
      } finally {
        try {
          if (stmt != null) {
            stmt.close();
          }
        } catch (Exception exception) {
        } finally {
          try {
            if (con != null) {
              con.close();
            }
          } catch (Exception exception) {
          } finally {
          }
        }
      }
    }

  } //

  //
  public static void ListDirs(Deque<String> stack, FileFilter dirFilter, String DirName)
      throws IOException {

    // get a list of dir(s) under the dir
    File dir = new File(DirName);
    File[] files = dir.listFiles(dirFilter);

    //
    if (files != null) {
      // iterate all dir(s)
      for (short si = 0, sj = (short) files.length; si < sj; si++) {
        //
        stack.addFirst(DirName + "\\" + files[si].getName());
      }
    }

  } //

  // init array
  public static void initArray(String[] theArray) {
    //
    for (short si = 0; si < theArray.length; si++) {
      //
      theArray[si] = "";
    }
  } //

  //
  public static void dumpHashMap(HashMap<String, HashMap<String, String>> theHashMap) {

    //
    Set<Map.Entry<String, HashMap<String, String>>> set1 = theHashMap.entrySet();
    Iterator<Map.Entry<String, HashMap<String, String>>> it1 = set1.iterator();
    while (it1.hasNext()) {
      //
      Map.Entry<String, HashMap<String, String>> pairs1 = it1.next();
      //
      Set<Map.Entry<String, String>> set2 = pairs1.getValue().entrySet();
      Iterator<Map.Entry<String, String>> it2 = set2.iterator();
      while (it2.hasNext()) {
        //
        Map.Entry<String, String> pairs2 = it2.next();
        log.info(pairs2.getKey() + "=" + pairs2.getValue());
        //
        // it2.remove();
      }
      //
      // it1.remove();
    }

  } //

}
