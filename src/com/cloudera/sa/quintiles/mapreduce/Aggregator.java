package com.cloudera.sa.quintiles.mapreduce;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.DelegatingInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.cloudera.sa.quintiles.mapreduce.mapper.XMLToSequenceFileMapper;

public class Aggregator extends Configured implements Tool {

  public static Job parseInputAndOutput(Tool tool, Configuration conf, String[] args)
      throws IOException {
    if (args.length != 2) {
      printUsage(tool, "filelist outpu_folder");
      return null;
    }
    final Job job = Job.getInstance(conf, "Aggregate " + args[1]);
    job.setJarByClass(tool.getClass());

    job.setInputFormatClass(WholeFileInputFormat.class);
    job.setOutputFormatClass(SequenceFileOutputFormat.class);

    WholeFileInputFormat.setInputPaths(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));

    //FileOutputFormat.setCompressOutput(job, true);
    //FileOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);

    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);
    job.setMapperClass(XMLToSequenceFileMapper.class);

    return job;
  }

  public static void printUsage(Tool tool, String extraArgsUsage) {
    System.err.printf("Usage: %s [genericOptions] %s\n\n", tool.getClass().getSimpleName(),
      extraArgsUsage);
    GenericOptionsParser.printGenericCommandUsage(System.err);
  }

  public static void main(String[] args) throws Exception {
    int exitCode = ToolRunner.run(new Aggregator(), args);
    System.exit(exitCode);
  }

  public int run(String[] args) throws Exception {

    Job job = parseInputAndOutput(this, getConf(), args);
    if (job == null) {
      return -1;
    }

    return job.waitForCompletion(true) ? 0 : 1;
  }

}
