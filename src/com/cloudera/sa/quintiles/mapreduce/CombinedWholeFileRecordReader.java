package com.cloudera.sa.quintiles.mapreduce;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.CombineFileSplit;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

class CombinedWholeFileRecordReader extends RecordReader<NullWritable, Text> {

  private CombineFileSplit split;
  private TaskAttemptContext context;
  private int index;
  private WholeFileRecordReader wfrr;


  public CombinedWholeFileRecordReader(CombineFileSplit split, TaskAttemptContext context,
      Integer index) throws IOException {
    this.split = split;
    this.context = context;
    this.index = index;
    wfrr = new WholeFileRecordReader();
  }

  @Override
  public void initialize(InputSplit split, TaskAttemptContext context) throws IOException,
      InterruptedException {
    this.split = (CombineFileSplit) split;
    this.context = context;
    if (null == wfrr) {
      wfrr = new WholeFileRecordReader();
    }
    FileSplit fileSplit =
        new FileSplit(this.split.getPath(index), this.split.getOffset(index),
            this.split.getLength(index), this.split.getLocations());
    this.wfrr.initialize(fileSplit, this.context);
  }
  
  @Override
  public boolean nextKeyValue() throws IOException, InterruptedException {
    return wfrr.nextKeyValue();
  }

  @Override
  public NullWritable getCurrentKey() throws IOException, InterruptedException {
    return wfrr.getCurrentKey();
  }

  @Override
  public Text getCurrentValue() throws IOException, InterruptedException {
    return wfrr.getCurrentValue();
  }

  @Override
  public float getProgress() throws IOException {
    return wfrr.getProgress();
  }

  @Override
  public void close() throws IOException {
    // do nothing
    if (wfrr != null) {
      wfrr.close();
    }
  }
}