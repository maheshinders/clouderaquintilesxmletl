package com.cloudera.sa.quintiles.mapreduce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.CombineFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.CombineFileRecordReader;
import org.apache.hadoop.mapreduce.lib.input.CombineFileSplit;
import org.apache.hadoop.mapreduce.lib.input.InvalidInputException;
import org.apache.hadoop.mapreduce.security.TokenCache;

public class CombinedWholeFileInputFormat extends
    CombineFileInputFormat<NullWritable, Text> {
  @Override
  protected boolean isSplitable(JobContext context, Path file) {
    return false;
  }

  @Override
  public RecordReader<NullWritable, Text> createRecordReader(InputSplit split,
      TaskAttemptContext context) throws IOException {
    return new CombineFileRecordReader<NullWritable, Text>((CombineFileSplit) split, context,
        CombinedWholeFileRecordReader.class);
  }
  

  @Override
  /** List input files. Assumption is that we are passing ONLY files... Filters are disabled!
   *
   * @param job the job to list input paths for
   * @return array of FileStatus objects
   * @throws IOException if zero items.
   */
  protected List<FileStatus> listStatus(JobContext job) throws IOException {
    List<FileStatus> result = new ArrayList<FileStatus>();
    Path[] dirs = getInputPaths(job);
    if (dirs.length == 0) {
      throw new IOException("No input paths specified in job");
    }

    // get tokens for all the required FileSystems..
    TokenCache.obtainTokensForNamenodes(job.getCredentials(), dirs,
                                        job.getConfiguration());

    List<IOException> errors = new ArrayList<IOException>();

    //Voir si ça passe d'en faire 40 000 d'un coups...
    //Ça devrait passer. Mais à tester...
    //if (true)
    //  return Arrays.asList(dirs[0].getFileSystem(job.getConfiguration()).listStatus(dirs));

    FileSystem fs = null;
    for (int i=0; i < dirs.length; ++i) {
      Path p = dirs[i];
      if (fs == null)
        fs = p.getFileSystem(job.getConfiguration());
      FileStatus[] matches = fs.listStatus(p);

      if (matches == null) {
        errors.add(new IOException("Input path does not exist: " + p));
      } else if (matches.length == 0) {
        errors.add(new IOException("Input Pattern " + p + " matches 0 files"));
      } else {
        for (FileStatus globStat: matches) {
          if (globStat.isDirectory()) {
            for(FileStatus stat: fs.listStatus(globStat.getPath())) {
              result.add(stat);
            }
          } else {
            result.add(globStat);
          }
        }
      }
    }

    if (!errors.isEmpty()) {
      throw new InvalidInputException(errors);
    }
    System.out.println (new Date () + " CombinedWholeFileInputFormat - Total input paths to process : " + result.size());
    return result;
  }
}