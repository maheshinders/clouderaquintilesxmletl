package com.cloudera.sa.quintiles.mapreduce;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.lib.MultipleOutputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.cloudera.sa.quintiles.mapreduce.mapper.XMLToOMOPMapper;
import com.cloudera.sa.quintiles.mapreduce.reducer.OneToOneReducer;

public class Conversion extends Configured implements Tool {

  public static Job parseInputAndOutput(Tool tool, Configuration conf, String[] args)
      throws IOException {

    if (args.length != 2) {
      printUsage(tool, "input_folder output_folder");
      return null;
    }

    final Job job = Job.getInstance(conf, "Conversion");
    job.setJarByClass(tool.getClass());

    return job;
  }

  public static void printUsage(Tool tool, String extraArgsUsage) {
    System.err.printf("Usage: %s [genericOptions] %s\n\n", tool.getClass().getSimpleName(),
      extraArgsUsage);
    GenericOptionsParser.printGenericCommandUsage(System.err);
  }

  public static void main(String[] args) throws Exception {
    int exitCode = ToolRunner.run(new Conversion(), args);
    System.exit(exitCode);
  }

  public int run(String[] args) throws Exception {

    Job job = parseInputAndOutput(this, getConf(), args);
    if (job == null) {
      return -1;
    }

    //job.getConfiguration().set("mapreduce.jobtracker.address", "local");
    //job.getConfiguration().set("fs.defaultFS", LocalFileSystem.DEFAULT_FS);


    SequenceFileInputFormat.setInputPaths(job, new Path(args[0]));
    TextOutputFormat.setOutputPath(job, new Path(args[1]));

    MultipleOutputs.addNamedOutput(job, "caresite", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "cohort", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "conditionera", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "conditionoccurrence", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "death", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "drugcost", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "drugera", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "drugexposure", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "location", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "observation", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "observationperiod", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "organization", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "payerplanperiod", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "person", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "procedurecost", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "procedureoccurrence", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "provider", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(job, "visitoccurrence", TextOutputFormat.class, Text.class, Text.class);

    job.setInputFormatClass(SequenceFileInputFormat.class);
    job.setOutputFormatClass(TextOutputFormat.class);

    job.setMapOutputKeyClass(Text.class);
    job.setMapOutputValueClass(Text.class);

    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(NullWritable.class);

    job.setMapperClass(XMLToOMOPMapper.class);
    //job.setReducerClass(OneToOneReducer.class);

    job.setNumReduceTasks(0);

    return job.waitForCompletion(true) ? 0 : 1;
  }

}
